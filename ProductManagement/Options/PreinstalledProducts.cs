﻿using System.Collections.Generic;
using ProductManagement.Models;

namespace ProductManagement.Options
{
    public class PreinstalledProducts
    {
        public List<Product> Products { get; set; }
    }
}