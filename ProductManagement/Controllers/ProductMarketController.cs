﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ProductManagement.Models;
using ProductManagement.Services;

namespace ProductManagement.Controllers
{
    [ApiController]
    [Route("api/[Controller]/[Action]")]
    public class ProductMarketController : ControllerBase
    {
        private readonly ProductService _service;

        private readonly ILogger<ProductMarketController> _logger;
        private readonly ProductService _productService;

        public ProductMarketController(ILogger<ProductMarketController> logger, ProductService service, ProductService productService)
        {
            _logger = logger;
            _service = service;
            _productService = productService;
        }

        /// <summary>
        /// Получить список всех продуктов
        /// </summary>
        [HttpGet]
        public List<Product> GetAllProducts()
        {
            return _productService.GetProducts();
        }
        /// <summary>
        /// Получить продукт по id
        /// </summary>
        /// <param name="id">id продукта</param>
        [HttpGet]
        public Product GetProductById(int id)
        {
            return _productService.GetProduct(id);
        }
        /// <summary>
        /// Добавить новый продукт в список
        /// </summary>
        /// <param name="name">название продукта</param>
        /// <param name="type">тип продукта</param>
        /// <param name="price">цена продукта</param>
        [HttpPost]
        public void CreateNewProduct(string name, string type, double price)
        {
            _productService.AddProduct(name,type, price);
        }
        /// <summary>
        /// Обновить информацию о продукте
        /// </summary>
        /// <param name="id">id продукта</param>
        /// <param name="name">название продукта</param>
        /// <param name="type">тип продукта</param>
        /// <param name="price">цена продукта</param>
        [HttpPut]
        public void UpdateProduct(int id, string name, string type, double price)
        {
            _productService.UpdateProduct(id, name, type, price);
        }
        /// <summary>
        /// Удалить продукт по id
        /// </summary>
        /// <param name="id">id продукта</param>
        [HttpDelete]
        public void DeleteProduct(int id)
        {
            _productService.DeleteProduct(id);
        }
    }
}
