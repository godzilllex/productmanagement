﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Linq;
using ProductManagement.Models;
using ProductManagement.Options;
using Products = System.Collections.Generic.List<ProductManagement.Models.Product>;

namespace ProductManagement.Services
{
    public class ProductService
    {
        private List<Product> Products { get; set; }
        private readonly IConfiguration _configuration;
        private readonly IMemoryCache _cache;
        public ProductService(IConfiguration configuration, IOptions<PreinstalledProducts> preinstalledProducts, IMemoryCache cache)
        {
            _configuration = configuration;
            _cache = cache;
            Products = preinstalledProducts.Value == null ? new List<Product>() : preinstalledProducts.Value.Products;
        }
        

        public void AddProduct(string name, string type, double price)
        {
            int id;
            id = Products.Count == 0 ? 1 : Products.Max(product => product.Id);
            Products.Add(new Product()
            {
                Id = id,
                Name = name,
                Type = type,
                Price = price,
            });
        }
        
        public void DeleteProduct(int id)
        {
            var key = CacheKeys.CachedProduct(id);
            if (_cache.TryGetValue(key, out Product result))
                return;
            var product = Products.FirstOrDefault(p => p.Id == id);
            Products.Remove(product);
        }
        public void UpdateProduct(int id, string name, string type, double price)
        {
            var key = CacheKeys.CachedProduct(id);
            var product = _cache.TryGetValue(key, out Product result) ? result : Products.FirstOrDefault(p => p.Id == id);
            if (product == null) return;
            product.Name = name;
            product.Type = type;
            product.Price = price;

        }
        public Product GetProduct(int id)
        {
            var key = CacheKeys.CachedProduct(id);
            var product = _cache.TryGetValue(key, out Product result) ? result : Products.FirstOrDefault(p => p.Id == id);
            return product;
        }
        public Products GetProducts()
        {
            return Products;
        }

    }
}