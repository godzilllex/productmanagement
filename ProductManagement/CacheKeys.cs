﻿using ProductManagement.Models;

namespace ProductManagement
{
    public class CacheKeys
    {
        public static string CachedProduct(int productId) => $"{productId.ToString()}";
    }
}